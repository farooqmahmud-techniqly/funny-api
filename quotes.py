def funny_quotes():
    return [{
        "author": "Elbert Hubbard",
        "quote": "Do not take life too seriously. You will never get out of it alive."
    },
        {
            "author": "Mark Twain",
            "quote": "Get your facts first, then you can distort them as you please."
        },
        {
            "author": "Robin Williams",
            "quote": "Why do they call it rush hour when nothing moves?"
        }
    ]
